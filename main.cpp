#define _DEBUG

#include <iostream>
#include <iterator>
#include <fstream>
#include "mpegstream.h"
#include <map>

template <typename T, typename R=std::char_traits<T> >
class basic_logstreambuf : public std::basic_istreambuf<T, R> {
    typedef std::basic_string<T> string_type;
    typedef R traits;
    typedef typename traits::int_type int_type;

    typedef public std::basic_istreambuf<T, R> this_type;

    string_type pbuf;


    using this_type::setp;
    using this_type::pptr;
    using this_type::pbase;

public:
    basic_logstreambuf() {
        setp(begin(), end());
    }
protected:

    int copy_to_string(char* buf, int len) {
        return 0;
    }

    int_type overflow(int_type c = traits::eof()) {
        auto n = pptr() - pbase();
                 
    }

    int sync() {
        auto n = pptr() - pbase();
        return n;
    }

    T* begin() {
        return &pbuf.front();
    }

    T* end() {
        return &pbuf.back() + 1;
    }
};

template <typename T, typename R=std::char_traits<T> >
class basic_logstream : public std::basic_ostream<T, R> {
    typedef basic_logstreambuf<T, R> buffer_type;
    buffer_type buf;
public:
    basic_logstream() 
    : std::basic_ostream<T,R>(&buf) 
    {
    }
};

auto main(int argc, char* argv[]) -> int{


    std::ifstream fin("elephants.ts", std::ios::binary);
    if(fin.is_open()) {
        std::map<uint16_t, std::ostream*> oss = {
            { 0x21, new std::ofstream("elephants-video.ts", std::ios::binary) },
            { 0x22, new std::ofstream("elephants-audio.ts", std::ios::binary) }
        };
        demux_ostream fout;
        fout.set_streams(oss);

        fin >> std::noskipws;

        std::copy(std::istream_iterator<char>(fin),
                std::istream_iterator<char>(),
                std::ostream_iterator<char>(fout));




        fin.close();
    }

}
