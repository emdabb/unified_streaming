#ifndef BITSTREAM_H_
#define BITSTREAM_H_

template <typename T, typename Traits=std::char_traits<T> >
class basic_bitbuffer : public std::basic_streambuf<T, Traits> {
public:
    basic_bitbuffer(uint8_t* data, size_t bits) {
        this->rdbuf(data, data, data + bits);
    }
};


template <typename T, typename Traits=std::char_traits<T> >
class basic_inbitstream : public std::istream<T, Traits> {
    typedef basic_bitbuffer<T, Traits> buffer_type;
    typedef std::istream<T, Traits> base_type;

    buffer_type _buf;
public:
    basic_inbitstream(uint8_t* data, size_t bits) 
    : base_type(&_buf)
    , _buf(data, bits)
    { 
        this->rdbuf(&_buf);
    }
};

template <typename T, typename Traits=std::char_traits<T> >
class basic_onbitstream : public std::ostream<T, Traits> {
};

#endif
