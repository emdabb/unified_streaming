#pragma once

// avoid clutter in global namespace
namespace {

    template <typename T>
    T mean(const std::valarray<T>& a) {
        return a.sum() / a.size();
    }

    template <class T>
    T const variance(std::valarray<T> const &v) {
        if (v.size() == 0)
            return T(0.0);
        T average = mean(v); //v.sum() / v.size();
        std::valarray<T> diffs = v-average;
        diffs *= diffs;
        return diffs.sum()/diffs.size();
    }

    template <typename T>
    std::valarray<T> linspace(float a, float b, int t) {
        std::valarray<T> res(t);
        T tt = static_cast<T>(t);
        for(int i=0; i < t; i++) {
            T d = static_cast<T>(i) / tt;
            res[i] = a + i * (b - a) / tt;
        }
        return res;
    }

    template <typename T>
    std::valarray<T> arange(float start, float step, float stop) {
        int size = (stop - start) / step;
        std::valarray<T> res(size);
        for(int i=0; i < size; i++) {
            res[i] = start + step * i;
        }
        return res;
    }


template <typename T>
std::slice_array<T> row(std::valarray<T>& arr, unsigned r, unsigned dim) {
    return arr[std::slice(r * dim, dim, 1)];
}

template <typename T>
std::slice_array<T> col(std::valarray<T>& arr, unsigned c, unsigned dim) {
    unsigned rows = arr.size() / dim;
    return arr[std::slice(c, rows, dim)];
}

std::size_t indexnd(const std::valarray<std::size_t>& dim, const std::valarray<std::size_t>& ix) {
    assert(dim.size() == ix.size());
    std::vector<std::size_t> muls = { 1 };
    std::partial_sum(std::begin(dim), std::end(dim) - 1, std::back_inserter(muls), 
            [&] (const std::size_t& a, const std::size_t& b) {
                return a * b;
            });
    return std::inner_product(begin(ix), std::end(ix), std::begin(muls), 0);
}

// @param - m rows of a
// @param - p cols of a <-> rows of b
// @param - q cols of b
// @returns p * q matrix
template <typename T>
std::valarray<T> matmul(std::valarray<T>& a, std::valarray<T>& b, unsigned m, unsigned p, unsigned q) {
    DEBUG_METHOD();
    std::valarray<T> c(m * q);
    for(auto i=0; i < m; i++) {
        const auto& yy = static_cast<std::valarray<T> >(a[std::slice(i * p, p, 1)]);
        for(auto j=0; j < q; j++) {
            const auto& xx = static_cast<std::valarray<T> >(b[std::slice(j, p, q)]);
            c[j + i * q] = (xx * yy).sum();
        }
    }
    return c;
}

struct tensor {
    std::valarray<float> data;
    std::valarray<int> dim;
};

} // namespace 

