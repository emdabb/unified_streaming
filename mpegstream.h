#pragma once

#include <iostream>
#include <map>

template <typename T, typename Traits=std::char_traits<T> >
class basic_demux_streambuf : public std::basic_streambuf<T, Traits> {
    typedef typename Traits::int_type int_type;
    typedef T char_type;
    typedef std::basic_streambuf<T, Traits> streambuf_type;
    static const int BUFFER_SIZE=188;
    char_type _buf[BUFFER_SIZE];
    using streambuf_type::pbase;
    using streambuf_type::pptr;
    using streambuf_type::epptr;
    using streambuf_type::setp;
    using streambuf_type::pbump;
    typedef Traits traits;

    std::map<uint16_t, std::ostream*> pids;
public:

    basic_demux_streambuf() {
        setp(_buf, _buf + BUFFER_SIZE - 1);
    }

    virtual ~basic_demux_streambuf() {
        sync();
    }

    int process_and_flush(){
        std::ptrdiff_t n = pptr() - pbase();
        std::cout << "n=" << n << std::endl;
        for(char* p = pbase(), *e = pptr(); p != e; p++) {
 
            std::cout << std::hex << (int)(*p & 0xff) << std::endl;           
            //std::cout << std::hex << *((int*)p) << std::endl;
        }
        //std::cout.write(pbase(), n);
        //return n > 0 ? (pbump(-n), n) : Traits::eof();
        //setp(_buf, _buf + BUFFER_SIZE);
        pbump(-n);
        return n;
    }

    int sync() {
        return process_and_flush() == Traits::eof() ? Traits::eof() : 0;
    }

    int_type overflow(int_type c = Traits::eof()) {
        if(!traits::eq_int_type(c, traits::eof())) {
            *pptr() = c;
            pbump(1);
            process_and_flush();

        }
        return traits::not_eof(c);
    }

};

template <typename T, typename Traits=std::char_traits<T> >
class basic_elementary_istreambuf : public std::basic_streambuf<T, Traits> {
public:    
};
/**
 * @brief 
 * An elementary stream (ES) as defined by the MPEG communication protocol is 
 * usually the output of an audio or video encoder. ES contains only one kind 
 * of data, e.g. audio, video or closed caption. 
 *
 * An elementary stream is often referred to as "elementary", "data", "audio", 
 * or "video" bitstreams or streams. 
 *
 * The format of the elementary stream depends upon the codec or data carried 
 * in the stream, but will often carry a common header when packetized into a 
 * packetized elementary stream.
 */
template <typename T, typename Traits=std::char_traits<T> >
class basic_demux_ostream : public std::basic_ostream<T, Traits> {
    typedef basic_demux_streambuf<T, Traits> buffer_type;
    buffer_type _buf;
public:
    basic_demux_ostream()
    : std::basic_ostream<T, Traits>(&_buf) 
    {
    }

	
    void set_streams(const std::map<uint16_t, std::ostream*>& streams) {
        for(auto& it : streams) {
            //_buf.append(it.first, it.second->rdbuf());
        }
    }
};


typedef basic_demux_ostream<char> demux_ostream;
