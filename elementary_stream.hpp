#pragma once

#include <iostream>

template <typename T, typename Traits=std::char_traits<T> >
class basic_elementary_ostreambuf : public std::basic_streambuf<T, Traits> {
    typedef typename Traits::int_type int_type;
    typedef T char_type;
    typedef std::basic_streambuf<T, Traits> streambuf_type;
    static const int BUFFER_SIZE=188;
    char_type _buf[BUFFER_SIZE];
    using streambuf_type::pbase;
    using streambuf_type::pptr;
    using streambuf_type::epptr;
    using streambuf_type::setp;
    using streambuf_type::pbump;
protected:

    int parse_adaption_field(char_type* ch, int n) {

        return n;
    }
    int parse_pts(char_type* ch, int n) {
        //std::cout << "0x" << std::hex << *((int32_t*)&ch[0]) << "(len="<<std::dec<<n<<")" << std::endl;
        if(ch[0] == 0x47) {
            int32_t pts_hdr = *((int32_t*)&ch[0]);

            int pid = (pts_hdr & 0x1fff00) >> 8;

            int adaptation_field_control = ((pts_hdr & 0x30) >> 4);
            int has_payload = adaptation_field_control & 0x1;
            int has_adapt = adaptation_field_control & 0x2;


            
            std::cout << "0x" << std::hex << pts_hdr << "(len="<<std::dec<<n<<")" << std::endl;
        }

        return n;
    }
public:

    basic_elementary_ostreambuf() {
        setp(_buf, _buf + BUFFER_SIZE);
    }

    int flush() {
        int n = pptr() - pbase();
        if(n == 0) {
            return 0;
        }
        parse_pts(pbase(), n) == n ? (pbump(-n), 0) : Traits::eof();
        return n;
    }
    int sync() {
        if(flush() == Traits::eof()) {
            return Traits::eof();
        }
        return 0;
    }
    int_type overflow(int_type c) {
        if(sync() == Traits::eof()) {
            return Traits::eof();
        }

        if(c != Traits::eof()) {
            *pptr() = c;
            pbump(1);
        }


        /*if(flush() == Traits::eof()) {
            return Traits::eof();
        }*/
        return 0;

    }
};

template <typename T, typename Traits=std::char_traits<T> >
class basic_elementary_istreambuf : public std::basic_streambuf<T, Traits> {
public:    
};
/**
 * @brief 
 * An elementary stream (ES) as defined by the MPEG communication protocol is 
 * usually the output of an audio or video encoder. ES contains only one kind 
 * of data, e.g. audio, video or closed caption. 
 *
 * An elementary stream is often referred to as "elementary", "data", "audio", 
 * or "video" bitstreams or streams. 
 *
 * The format of the elementary stream depends upon the codec or data carried 
 * in the stream, but will often carry a common header when packetized into a 
 * packetized elementary stream.
 */
template <typename T, typename Traits=std::char_traits<T> >
class basic_elementary_ostream : public std::basic_ostream<T, Traits> {
    typedef basic_elementary_ostreambuf<T, Traits> buffer_type;
    buffer_type _buf;
public:
    basic_elementary_ostream()
    : std::basic_ostream<T, Traits>(&_buf) 
    {
    }
};

template <typename T, typename Traits=std::char_traits<T> >
class basic_elementary_istream : public std::basic_istream<T, Traits> {
    typedef basic_elementary_istreambuf<T, Traits> buffer_type;
    buffer_type _buf;
public:
    basic_elementary_istream()
    : std::basic_istream<T, Traits>(&_buf) 
    {
    }
};

typedef basic_elementary_ostream<char> elementary_ostream;
typedef basic_elementary_istream<char> elementary_istream;
