#ifndef MPEG2_TS_H_INCLUDED_
#define MPEG2_TS_H_INCLUDED_

#define PKT_SIZE    188

#if defined(_MSC_VER)
# define __aligned_type(x)  __declspec(align(x))

#elif defined(__GNUC__)
# define __aligned_type(x)  __attribute__((aligned(x)))
# define __packed_type  __attribute__((packed))
#else
# define __aligned_type(x)
# define __packed_type()
#endif

#define __declare_aligned(T, n)     T __aligned_type(n)
#define __declare_packed(T) T __packed_type

namespace mpeg {

    static const int packet_size = 188;

    enum class ts_adapt : uint8_t {
        TS_DI = 0x80, // Set if current TS packet is in a discontinuity state with respect to either the continuity counter or the program clock reference
        TS_RAI = 0x40, // Set when the stream may be decoded without errors from this point
        TS_ESPI = 0x20, // Set when this stream should be considered "high priority"
        TS_PCR = 0x10, // Set when PCR field is present
        TS_OPCR = 0x08, // Set when OPCR field is present
        TS_SPLICE = 0x04, // Set when splice countdown field is present
        TS_PRIVATE = 0x02, // Set when private data field is present
        TS_EXT = 0x01 //    Set when extension field is present
    };
    enum class ts_pid_mask : uint32_t {
        PID_SYNC=0xff000000,
        PID_TEI = 0x800000,
        PID_PUSI = 0x400000,
        PID_PRIO = 0x200000,
        PID_PID = 0x1fff00
    };

    enum class ts_aux_mask : uint8_t {
        AUX_TSC = 0xc0,
        AUX_AFC = 0x30,
        AUX_CONT = 0xf
    };
    /**
    struct uint48_t {
        uint64_t x : 48;
    } __attribute__((packed));

    struct ts_adapt {
        uint8_t len; // 8 bits. Number of bytes in the adaptation field immediately following this byte
        uint8_t indicators; // 8 bits. bitmask of indicators as defined by ts_adapt_indicator
        uint48_t pcr; // 48 bits. Program clock reference, stored as 33 bits base, 6 bits reserved, 9 bits extension.        The value is calculated as base * 300 + extension.
        uint48_t opcr; // Original Program clock reference. Helps when one TS is copied into another
        uint8_t splice_countdown; // Indicates how many TS packets from this one a splicing point occurs (Two's complement signed; may be negative)
        uint8_t transport_private_data; 
    };


    struct ts_packet {
        uint8_t sync;
        uint16_t pid;
        uint8_t aux;
        ts_adapt adapt;

    };

    struct ts_mpeg {
        static const int MAX_BUFFER = 102400;
        uint8_t buf[MAX_BUFFER]; //gbuf / pbuf
        int pos; // gbuf / pbuf
        int num_pids;
        ts_pid pids[32];
        ts_pid* cur_pid;
        uint64_t pcr_last;
        ts_packet* cur_packet;
    };
    */
}

#endif // MPEG2_TS_H_INCLUDED_


